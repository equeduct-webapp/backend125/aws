package aws

import (
	"bytes"
	"fmt"
	"log"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"equeduct.com/module/data"
	"equeduct.com/module/util"
)

func ImageUploaderS3(formDetails data.UserDetails) {

	fmt.Println("Connecting to AWS S3 ...")

	session, err := session.NewSession(&aws.Config{
		Region:      aws.String(data.AWS_S3_REGION),
		Credentials: credentials.NewStaticCredentials(data.AWS_ACCESS_KEY_ID, data.AWS_SECRET_ACCESS_KEY, ""),
	})

	if err != nil {
		fmt.Println("Error in AWS S3 connection")
		log.Fatal(err)
	}

	frontID, format := util.Base64StringToBytes(formDetails.IdentificationFrontSide)

	fmt.Println("Starting upload to AWS S3 ...")

	if len(frontID) > 0 && format != "" {
		
		err = uploadImageS3(session, frontID, formDetails.IdentificationFrontSideName)
		if err != nil {
			fmt.Println("Failed to upload front ID ", err.Error())
		} else {
			fmt.Println("file uploaded successfully : ", formDetails.IdentificationFrontSideName)
		}
	}
	
	backID, format := util.Base64StringToBytes(formDetails.IdentificationBackSide)
	
	if len(backID) > 0 && format != "" {
		
		err = uploadImageS3(session, backID, formDetails.IdentificationBackSideName)
		if err != nil {
			fmt.Println("Failed to upload front ID ", err.Error())
		} else {
			fmt.Println("file uploaded successfully : ", formDetails.IdentificationBackSideName)
		}
	}
}

func uploadImageS3(session *session.Session, imageBytes []byte, uploadFileName string) error {

	_, err := s3.New(session).PutObject(&s3.PutObjectInput{
		Bucket: aws.String(data.AWS_S3_BUCKET),
		Key:    aws.String(uploadFileName),
		ACL:    aws.String("private"),
		Body:   bytes.NewReader(imageBytes),
		// ContentLength:        aws.Int64(fileSize),
		ContentType: aws.String(http.DetectContentType(imageBytes)),
	})
	return err
}
